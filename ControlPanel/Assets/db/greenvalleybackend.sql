-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.3-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for greenvalley
CREATE DATABASE IF NOT EXISTS `greenvalley` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `greenvalley`;

-- Dumping structure for table greenvalley.banner
CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `link` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `max_display` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.banner: ~12 rows (approximately)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` (`id`, `title`, `picture`, `link`, `is_active`, `max_display`, `created_at`, `modified_at`) VALUES
	(3, 'title_3', 'blueberries.jpg', 'link_3', 1, 4, '2018-07-31 23:48:14', '2018-07-31 23:48:17'),
	(8, 'title8', 'b3.png', 'link8', 0, NULL, '2018-08-01 13:38:00', NULL),
	(9, 'vegetables', 'b4.jpg', 'fsdrgdfhbfgh', 1, NULL, NULL, NULL),
	(11, 'fruitsm', 'berries.jpg', 'hjmhg', 1, NULL, NULL, NULL),
	(12, 'fruitssss', 'raspberry.jpg', 'ujugjghfhyf', 0, NULL, '2018-08-01 13:38:00', NULL),
	(14, 'ffff', 'kiwi.jpg', 'gjgyhkg', 0, NULL, '2018-08-01 13:38:08', NULL),
	(15, 'kjiljkolj', 'berries.jpg', 'hjkhklh', 0, NULL, NULL, NULL),
	(16, 'banana', 'banana.png', 'jmbm', 1, NULL, NULL, NULL),
	(17, 'mmmmm', 'orange.jpg', 'ghjgbjghj', 0, NULL, NULL, NULL),
	(19, 'title18', 'grapes.jpg', 'link18', 1, NULL, NULL, NULL),
	(20, 'title19', 'avocado.jpg', 'link19', 0, NULL, NULL, NULL),
	(21, 'mangoes', 'mangoes.jpg', 'link_mango', 0, NULL, NULL, NULL);
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

-- Dumping structure for table greenvalley.registration
CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(32) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.registration: ~6 rows (approximately)
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
INSERT INTO `registration` (`id`, `name`, `email`, `password`, `phone`, `created_at`, `modified_at`) VALUES
	(1, 'Joya', 'js@gmail.com', 'joya', '018187435654', '2018-07-31 12:16:50', '2018-07-31 12:17:02'),
	(2, 'irin', 'i@bd.com', '', '0185464657', '2018-08-01 11:58:26', NULL),
	(3, 'Rashna', 'r@bd.com', 'rashna', '056567568', '2018-08-01 11:58:33', NULL),
	(12, 'Owakil', 'md@bd.com', 'owakil', '0174677879', '2018-08-01 11:58:36', NULL),
	(13, 'Fahima', 'fa@bd.com', 'fahima', '018376646', '2018-08-01 11:58:38', NULL),
	(14, 'Orchi', 'orchi@gmail.com', 'orchi', '0195786789', NULL, NULL);
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
