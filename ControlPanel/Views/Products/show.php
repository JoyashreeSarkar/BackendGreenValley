<?php
session_start();
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/GreenValley/Model/vendor/Autoload.php';

use Joya\Utility\Debug;
use Joya\Product\Product;
use Joya\Utility\AppConfig;

$appConfig = new AppConfig();
$objproduct = new Product();
$product = $objproduct->show($_GET['id']);
?>
    <?= $appConfig->backElementPath('navbar.php'); ?>
    <h1>All Information of this Product</h1>

    <div class="form-group row">
        <label for="product_name" class="col-sm-2 col-form-label">Product Name:</label>
        <div class="col-sm-10">
            <?= $product['product_name'] ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="product_code" class="col-sm-2 col-form-label">Product Code:</label>
        <div class="col-sm-10">
            <?= $product['product_code'] ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="picture" class="col-sm-2 col-form-label">Picture:</label>
        <div class="col-sm-10">
            <img src="http://localhost/GreenValley/products/<?php echo $product['picture'] ?>" height="180" width="300"
                 style="border-radious:30px;">
        </div>
    </div>
    <div class="form-group row">

        <label class="col-sm-2 col-form-label" for="categories">Categories:</label>
        <div class="col-sm-10">
            <?= $product['categories'] ?>
        </div>

    </div>
    <div class="form-group row">
        <label for="quantity" class="col-sm-2 col-form-label">Quantity:</label>
        <div class="col-sm-10">
            <?= $product['quantity'] ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="price" class="col-sm-2 col-form-label">Price:</label>
        <div class="col-sm-10">
            <?= $product['price'] ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="discount" class="col-sm-2 col-form-label">Discount:</label>
        <div class="col-sm-10">
            <?= $product['discount'] ?>
        </div>
    </div>
    <div class="form-group row">
        <label for="description" class="col-sm-2 col-form-label">Description:</label>
        <div class="col-sm-10">
            <?= $product['description'] ?>
        </div>
    </div>


<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>