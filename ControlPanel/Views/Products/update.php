<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Debug;
use Joya\Utility\Validator;
use Joya\Product\Product;
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();

move_uploaded_file($_FILES['picture']['tmp_name'],"../../../../products/".$_FILES['picture']['name']);
//collect from data
$data=$_POST;

$product_name = Validator::validate($data['product_name']);
$product_code = Validator::validate($data['product_code']);
$picture=Validator::validate($_FILES['picture']['name']);
$data['picture'] = $picture;
$categories=Validator::validate($data['categories']);
$quantity=Validator::validate($data['quantity']);
$price=Validator::validate($data['price']);
$discount=Validator::validate($data['discount']);
$description=Validator::validate($data['description']);
$id= $data['id'] ;

$product = new Product();
$product->update($data);
