<?php
session_start();
//if ($_SESSION['user'] != true) {
//    header('location:http://localhost/GreenValley/ControlPanel/Views/ThemeA/Login/login.php');
//}
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'] . '/GreenValley/Model/vendor/Autoload.php';
$dbh = new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");

use Joya\Utility\AppConfig;

$appConfig = new AppConfig();
$query2 = "SELECT * FROM categories  ORDER BY  `title` ASC ";
$products2 = $dbh->query($query2);


?>

    <?= $appConfig->backElementPath('navbar.php'); ?>
    <form method="post" action="store.php" role="form" enctype="multipart/form-data">
        <div class="form-group row">
            <label for="product_name" class="col-sm-2 col-form-label">Product Name</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="product_name" name="product_name">
            </div>
        </div>
        <div class="form-group row">
            <label for="product_code" class="col-sm-2 col-form-label">Product Code</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="product_code" name="product_code">
            </div>
        </div>
        <div class="form-group row">
            <label for="picture" class="col-sm-2 col-form-label">Picture</label>
            <div class="col-sm-10">
                <input type="file" class="form-control-file border" id="picture" name="picture">
            </div>
        </div>
        <div class="form-group row">
            <div class="input-group mb-3">
                <div class="input-group-append">
                    <label class="input-group-text" for="categories">Categories</label>
                </div>
                <select class="custom-select" id="categories" name="categories">
                    <option selected>Choose...</option>
                    <option value="Cancer">Cancer</option>
                    <option value="Diabetes">Diabetes</option>
                    <option value="High Blood Pressure">High Blood Pressure</option>
                    <option value="Low Blood Pressure">Low Blood Pressure</option>
                    <option value="Heart Attack">Heart Attack</option>
                    <option value="Lung Cancer">Lung Cancer</option>
                    <option value="Pregnant Lady">Pregnant Lady</option>
                    <option value="Brain Anserum">Brain Anserum</option>
                    <option value="Hepaticies Cr">Hepaticies C</option>
                </select>

            </div>

        </div>
        <div class="form-group row">
            <label for="quantity" class="col-sm-2 col-form-label">Quantity</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="quantity" name="quantity" placeholder=" Kg/dozzen">
            </div>
        </div>
        <div class="form-group row">
            <label for="price" class="col-sm-2 col-form-label">Price</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="price" name="price" placeholder="tk. Per Kg/dozzen">
            </div>
        </div>
        <div class="form-group row">
            <label for="discount" class="col-sm-2 col-form-label">Discount</label>
            <div class="col-sm-10">
                <input type="number" class="form-control" id="discount" name="discount" placeholder="Per Kg/dozzen">
            </div>
        </div>
        <div class="form-group row">
            <label for="description" class="col-sm-2 col-form-label">Description</label>
            <div class="col-sm-10">
                <input type="textarea" class="form-control" id="description" name="description">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Save</button>

            </div>
        </div>
    </form>



<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>




