<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';

use Joya\Utility\Validator;
use Joya\Product\Product;
move_uploaded_file($_FILES['picture']['tmp_name'],"../../../../products/".$_FILES['picture']['name']);

$data=[];
$data['product_name'] = Validator::validate($_POST['product_name']);
$data['product_code'] = Validator::validate($_POST['product_code']);
$data['picture'] = Validator::validate($_FILES['picture']['name']);
$data['categories'] = Validator::validate($_POST['categories']);
$data['quantity'] = Validator::validate($_POST['quantity']);
$data['price'] = Validator::validate($_POST['price']);
$data['discount'] = Validator::validate($_POST['discount']);
$data['description'] = Validator::validate($_POST['description']);
$data['created_at'] = date('Y-m-d h:i:s',time());
$data['modified_at'] = date('Y-m-d h:i:s',time());

$product = new Product();
$product->store($data);