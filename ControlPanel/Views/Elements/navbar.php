<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/GreenValley/Model/vendor/Autoload.php';

?>
<div style="text-align: right;padding-right: 30px">
    <a  href="http://localhost/GreenValley/ControlPanel/Views/ThemeA/Login/logout.php">Logout</a>
</div>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="http://localhost/GreenValley/ControlPanel/Views/home.php">GREEN VALLEY</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Banner/create.php">Create Banner <span
                            class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Banner/index.php">All Banner List </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Banner/active_banners.php">Active
                    Banner List </a>
            </li>
<!--            <li class="nav-item active">-->
<!--                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Banner/trash.php">Trash <span-->
<!--                            class="sr-only">(current)</span></a>-->
<!--            </li>-->
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Registration/create.php">Registration
                    Form<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Registration/index.php">Registered
                    Person <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Products/create.php">Add New Products
                    <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="http://localhost/GreenValley/ControlPanel/Views/Products/index.php">All Products <span
                            class="sr-only">(current)</span></a>
            </li>

        </ul>
        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>









