<?php
session_start();
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Debug;
use Joya\Utility\Message;
use Joya\Registration\Registration;
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();

$objregistration = new Registration();
$registration = $objregistration->show($_GET['id']);

?>
  <?= $appConfig->backElementPath('navbar.php'); ?>
    <form method="post" action="update.php" role="form">
	<input id="id" type="hidden" name="id" value="<?= $registration['id'] ?>" class="form-control">
  <div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Full Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="name" name="name" value="<?= $registration['name'] ?>">
    </div>
   </div>
        <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
       <input type="text" class="form-control-file border"  id="email" name="email" value="<?= $registration['email'] ?>">
    </div>
    </div>
        <div class="form-group row">
            <label for="password" class="col-sm-2 col-form-label">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control"  id="password" name="password"value="<?= $registration['password'] ?>">
            </div>
        </div>
  <div class="form-group row">
    <label for="phone" class="col-sm-2 col-form-label">Phone</label>
    <div class="col-sm-10">
      <input type="tel" class="form-control" id="phone" name="phone" value="<?= $registration['phone'] ?>">
    </div>
  </div>
  
  
  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Update</button>

    </div>
  </div>
</form>

<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>
