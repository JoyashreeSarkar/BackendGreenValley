<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Validator;
use Joya\Registration\Registration;

//collect from data
$data=$_POST;
$name=Validator::validate($data['name']);
$email=Validator::validate($data['email']);
$password=Validator::validate($data['password']);
$phone=Validator::validate($data['phone']);

$id = $data['id'];
$registration= new Registration();
$registration->update($data);
