<?php
session_start();

ob_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();

?>
  <?= $appConfig->backElementPath('navbar.php'); ?>
    <form method="post" action="store.php" role="form">
  <div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
      <input type="text" class="form-control"  id="name" name="name">
    </div>
  </div>
   <div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
      <input type="email" class="form-control" id="email" name="email">
    </div>
  </div>
  <div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">Password</label>
    <div class="col-sm-10">
      <input type="password" class="form-control"  id="password" name="password">
    </div>
  </div>
  <div class="form-group row">
    <label for="phone" class="col-sm-2 col-form-label">Phone</label>
    <div class="col-sm-10">
      <input type="tel" class="form-control" id="phone" name="phone" >
    </div>
  </div>
  
  
  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">OK</button>

    </div>
  </div>
</form>

<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>





