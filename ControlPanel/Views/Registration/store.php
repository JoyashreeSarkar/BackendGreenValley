<?php
ini_set('display_errors','On');
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';

use Joya\Utility\Validator;
use Joya\Registration\Registration;

$data=[];
$data['name'] = Validator::validate($_POST['name']);
$data['email'] = Validator::validate($_POST['email']);
//$data['password']=Validator::validate($_POST['password']);
$data['phone']=Validator::validate($_POST['phone']);
$data['created_at']=date('Y-m-d h:i:s',time());
$data['modified_at']=date('Y-m-d h:i:s',time());
$data['password']=password_hash($_POST['password'],PASSWORD_DEFAULT);

$registration = new Registration();
$registration->store($data);