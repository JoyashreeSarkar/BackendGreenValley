
<?php
session_start();
//if($_SESSION['user']!=true){
//    header('location:http://localhost/GreenValley/ControlPanel/Views/Login/login.php');
//}
ob_start();

include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Debug;
use Joya\Utility\Message;
use Joya\Contact\Contact;
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();
$contact = new Contact();
$contacts = $contact->all_message();

?>

<?= $appConfig->backElementPath('navbar.php'); ?>
<?php
$message = new Message();
if($message->has()): ?>
    <div class="alert alert-success">
        <?php
        echo $message->get();
        ?>
    </div>
<?php endif; ?>

<div class="row">
    <div class="offset-10">
        <a href="trash.php"><img src="http://localhost/GreenValley/images/trash.jpg" height="30px" width="30px"> </a>
        <a href="seen_message.php"><img src="http://localhost/GreenValley/images/message.jpg" height="30px" width="30px"> </a>
    </div>
    <div class="col-lg-11 col-lg-offset-1">

        <table class="table table-striped">
            <thead>
            <tr>
                <!--                    <th scope="col">ID</th>-->
                <th scope="col">Full Name</th>
                <th scope="col">Email</th>
                <th scope="col">Message</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($contacts as $contact){ ?>
                <tr>
                    <!--                    <th scope="$registration">--><?php //echo $contact['id']?><!--</th>-->
                    <td><?php echo $contact['name'] ?></td>
                    <td><?php echo $contact['email']?></td>
                    <td><?php echo $contact['message']?></td>
                    <td><a href="show.php?id=<?=$contact['id']; ?>">Show</a> |
                        <a href="destroy.php?id=<?=$contact['id']; ?>" onclick="return confirm('Are you sure you want to delete item?')">Delete</a>|
                        <?php
                        if($contact['is_seen']){
                            echo '<a href="read.php?id='.$contact['id'].'"onclick="return confirm(\'You want to unread this message?\')"><img src="http://localhost/GreenValley/images/read_message.png" height="30px" width="30px"></a>';


                        }else{
                            echo '<a href="unread.php?id='.$contact['id'].'"onclick="return confirm(\'You want to read this message?\')"><img src="http://localhost/GreenValley/images/unread_message.png" height="30px" width="30px"></a>';

                        }
                        ?>
                    </td>
                </tr>
            <?php } ?>

            </tbody>
        </table>
    </div>
</div>

<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>

