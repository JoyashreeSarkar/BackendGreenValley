
<?php
ini_set('display_errors','On');
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';

use Joya\Utility\Debug;
use Joya\Utility\Validator;
use Joya\Contact\Contact;
$data=[];
$data['name'] = Validator::validate($_POST['name']);
$data['email'] = Validator::validate($_POST['email']);
$data['message']=Validator::validate($_POST['message']);
$data['created_at']=date('Y-m-d h:i:s',time());
$data['modified_at']=date('Y-m-d h:i:s',time());
$contact = new Contact();
$contact->store($data);