<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Debug;
use Joya\Utility\Validator;
use Joya\Banner\Banner;
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();

move_uploaded_file($_FILES['picture']['tmp_name'],"../../../../banner/".$_FILES['picture']['name']);
//collect from data
$data=$_POST;

$title = Validator::validate($data['title']);
$picture=Validator::validate($_FILES['picture']['name']);
$data['picture'] = $picture;
$link=Validator::validate($data['link']);
$is_active=Validator::validate($data['is_active']);
$id= $data['id'] ;

$banner= new Banner();
$banner2= new Banner();
$banner->update($data);
$banner2->updateActive($data);
