<?php

session_start();
//if($_SESSION['user']!=true){
//    header('location:http://localhost/GreenValley/ControlPanel/Views/ThemeA/Login/login.php');
//}
ob_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\AppConfig;
use Joya\Utility\Message;

$message=new Message();
$appConfig = new AppConfig();



?>

  <?= $appConfig->backElementPath('navbar.php'); ?>
    <form method="post" action="store.php" role="form" enctype="multipart/form-data">
  <div class="form-group row">
    <label for="title" class="col-sm-2 col-form-label">Title<span class="text-danger">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="title" name="title" >

        <?php
        if($message->has()){
            ?>
            <div class="alert alert-warning">
                <?php
                echo $message;
                ?>
            </div>
        <?php }
        ?>

    </div>
  </div>
        <div class="form-group row">
            <label for="picture" class="col-sm-2 col-form-label">Picture</label>
            <div class="col-sm-10">
            <input type="file" class="form-control-file border"  id="picture" name="picture">
            </div>
        </div>
  <div class="form-group row">
    <label for="link" class="col-sm-2 col-form-label">Link<span class="text-danger">*</span></label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="link" name="link">
        <?php if($message->has()){  ?>
            <div class="alert alert-warning">
                <?php
                echo $message;
                ?>
            </div>
        <?php }
        ?>
    </div>
  </div>
  
  <fieldset class="form-group">
    <div class="row">
      <legend class="col-form-label col-sm-2 pt-0">Active</legend>
      <div class="col-sm-10">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="is_active" id="is_active" value="1" checked>
          <label class="form-check-label" for="is_active">
           Yes
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="is_active" id="is_active" value="0">
          <label class="form-check-label" for="is_active">
            No
          </label>
        </div>
      </div>
    </div>
  </fieldset>
  <div class="form-group row">
    <div class="col-sm-10">
      <button type="submit" class="btn btn-primary">Save</button>

    </div>
  </div>
</form>


<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>

