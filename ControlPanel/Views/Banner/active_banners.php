
<?php
session_start();
ob_start();

include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Debug;
use Joya\Utility\Message;
use Joya\Banner\Banner;
use Joya\Utility\AppConfig;

$banner=new Banner();
$banners = $banner->bannerActive();
$appConfig = new AppConfig();




?>

  <?= $appConfig->backElementPath('navbar.php'); ?>
   <?php 
  $message = new Message();
	if($message->has()): ?>
		<div class="alert alert-success">
			<?php
			echo $message->get();
			?>
		</div>
   <?php endif; ?>

    <div class="row">

        <div class="col-lg-10 col-lg-offset-2">

            <h1>Banner List</h1>
            <table class="table table-striped">
                <thead>
                <tr>
<!--                    <th scope="col">ID</th>-->
                    <th scope="col">Title</th>
                    <th scope="col">Picture</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($banners as $banner){ ?>
                <tr>
<!--                    <th scope="$banner">--><?php //echo $banner['id']?><!--</th>-->
                    <td><?php echo $banner['title'] ?></td>
                    <td><img src="http://localhost/GreenValley/banner/<?php echo $banner["picture"] ?>" height="120" width="400" style="border-radious:30px;" ></td>
                    <td><a href="show.php?id=<?=$banner['id']; ?>">Show</a> |
                       <a href="softDestroy.php?id=<?=$banner['id']; ?>" onclick="return confirm('Are you sure you want to delete item?')">Delete</a>|
                       <a href="edit.php?id=<?=$banner['id']; ?>">Edit</a>|
					</td>
                </tr>
                <?php } ?>

                </tbody>
            </table>
        </div>

    </div>

</div>


<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>