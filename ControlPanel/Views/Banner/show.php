<?php
session_start();
ob_start();

include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Debug;
use Joya\Banner\Banner;
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();
$objbanner = new Banner();
$banner = $objbanner->show($_GET['id']);


?>

  <?= $appConfig->backElementPath('navbar.php'); ?>
 <h1>All Information of a Banner</h1>


  <div class="form-group row">
      <label for="title" class="col-sm-2 col-form-label">Title:</label>
      <div class="col-sm-10">
          <?= $banner['title']?>
      </div>
  </div>
  <div class="form-group row">
      <label for="picture" class="col-sm-2 col-form-label">Picture:</label>
      <div class="col-sm-10">
          <img src="http://localhost/GreenValley/banner/<?php echo $banner["picture"] ?>" height="120" width="400" style="border-radious:30px;" >
      </div>
  </div>
  <div class="form-group row">
      <label for="link" class="col-sm-2 col-form-label">Link:</label>
      <div class="col-sm-10">
          <?= $banner['link']?>
      </div>
  </div>

  <fieldset class="form-group">
      <div class="row">
          <legend class="col-form-label col-sm-2 pt-0">Active:</legend>
          <div class="col-sm-10">
              <?php
              if( $banner['is_active']){
                  echo '<a href= "deactivate.php?id='.$banner['id'].'" onclick="return confirm(\'You want to deactivate this banner?\')">Deactivate</a>';
              }else{
                  echo '<a href= "activate.php?id='.$banner['id'].'"onclick="return confirm(\'You want to activate this banner?\')">Activate</a>';
              }
              ?>
          </div>
      </div>
  </fieldset>


<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>
