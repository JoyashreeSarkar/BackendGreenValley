<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';

use Joya\Banner\Banner;
use Joya\Utility\Debug;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;

$banner=new Banner();
$banners=$banner->index('banners');

$spreadsheet = new Spreadsheet();  /*----Spreadsheet object-----*/
$Excel_writer = new Xls($spreadsheet);  /*----- Excel (Xls) Object*/
$spreadsheet->setActiveSheetIndex(0);
$activeSheet = $spreadsheet->getActiveSheet();
$spreadsheet->setActiveSheetIndex(0)
    ->setCellValue('A1', 'Title')
    ->setCellValue('B1', 'Link')
    ->setCellValue('C1', 'Picture');
$i=2;
foreach ($banners as $banner){
    $spreadsheet->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, $banner['title'])
        ->setCellValue('B'.$i, $banner['link'])
        ->setCellValue('C'.$i, $banner['picture']);
    $i++;
}
$filename = 'Banners_' . time();
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="' . $filename . '.xls"'); /*-- $filename is  xsl filename ---*/
header('Cache-Control: max-age=0');
$Excel_writer->save('php://output');