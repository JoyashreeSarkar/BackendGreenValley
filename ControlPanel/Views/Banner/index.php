
<?php
session_start();
//if($_SESSION['user']!=true){
//    header('location:http://localhost/GreenValley/ControlPanel/Views/ThemeA/Login/login.php');
//}
ob_start();

include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\Debug;
use Joya\Utility\Message;
use Joya\Banner\Banner;
use Joya\Utility\AppConfig;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
$banner=new Banner();
$banners = $banner->index();

//Debug::dd($products);
//die();
if (array_key_exists('page', $_GET)) {
    $currentPage = $_GET['page'];
} else {
    $currentPage = 1;
}
$adapter = new ArrayAdapter($banners);
//var_dump($adapter);
//die();
$pagerfanta = new Pagerfanta($adapter);
$pagerfanta->setMaxPerPage(2); // 10 by default
$maxPerPage = $pagerfanta->getMaxPerPage();

$pagerfanta->setCurrentPage($currentPage); // 1 by default
$currentPage = $pagerfanta->getCurrentPage();

$nbResults = $pagerfanta->getNbResults();//total records in database
$currentPageResults = $pagerfanta->getCurrentPageResults();//result based on current page number

//$pagerfanta->getNbPages();
//
//$pagerfanta->haveToPaginate(); // whether the number of results is higher than the max per page
//
//$pagerfanta->hasPreviousPage();
//$pagerfanta->getPreviousPage();
//$pagerfanta->hasNextPage();
//$pagerfanta->getNextPage();
//$pagerfanta->getCurrentPageOffsetStart();
//$pagerfanta->getCurrentPageOffsetEnd();
$appConfig = new AppConfig();
?>

  <div class="offset-11">
      <a href="trash.php"><img src="http://localhost/GreenValley/images/trash.jpg" height="30px" width="30px"> </a>
      <a href="download.php"><img src="http://localhost/GreenValley/images/download.jpg" height="30px" width="30px"></a>
  </div>
  <?= $appConfig->backElementPath('navbar.php'); ?>



  <?php
  $message = new Message();
	if($message->has()): ?>
		<div class="alert alert-success">
			<?php
			echo $message->get();
			?>
		</div>
   <?php endif; ?>

    <div class="row">

        <div class="col-lg-11 col-lg-offset-1">

            <h1>Banner List</h1>
            <table class="table table-striped">
                <thead>
                <tr>
<!--                    <th scope="col">ID</th>-->
                    <th scope="col">Title</th>
                    <th scope="col">Picture</th>
                    <th scope="col">Active</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($currentPageResults as $banner){ ?>
                <tr>
<!--                    <th scope="$banner">--><?php //echo $banner['id']?><!--</th>-->
                    <td><?php echo $banner['title'] ?></td>
                    <td><img src="http://localhost/GreenValley/banner/<?php echo $banner["picture"] ?>" height="120" width="400" style="border-radious:30px;" ></td>
                    <td>
                        <?php
                        if( $banner['is_active']){
                            echo '<a href= "deactivate.php?id='.$banner['id'].'" onclick="return confirm(\'You want to deactivate this banner?\')">Deactivate</a>';
                        }else{
                            echo '<a href= "activate.php?id='.$banner['id'].'"onclick="return confirm(\'You want to activate this banner?\')">Activate</a>';
                        }
                        ?>
                    </td>
                    <td><a href="show.php?id=<?=$banner['id']; ?>">Show</a> |
                        <a href="softDestroy.php?id=<?=$banner['id']; ?>" onclick="return confirm('Are you sure you want to delete item?')">Delete</a>|
                        <a href="edit.php?id=<?=$banner['id']; ?>">Edit</a>|
					</td>
                </tr>
                <?php
                }
                ?>

                </tbody>
            </table>
        </div>



    </div>
<div class="col-lg-6 offset-5">
    <ul class="pagination">
        <?php
        //$pageNumber=1;
//        if($pagerfanta->getPreviousPage()) {
//            echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/ControlPanel/Views/Banner/index.php?page=" . ($currentPage - 1) . "' class='button'><<</a></li>";
//        }
                    echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/ControlPanel/Views/Banner/index.php?page=" . ($currentPage - 1) . "' class='button'><<</a></li>";

        $totalPages = ceil($nbResults / $maxPerPage);
        if ($pagerfanta->haveToPaginate()) {
            for ($pageNumber = 1; $pageNumber <= $totalPages; $pageNumber++) {
                echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/ControlPanel/Views/Banner/index.php?page=" . $pageNumber . "'>" . $pageNumber . "</a></li>";
            }
        } else {
            echo "<li>No pagination required</li>";
        }
        echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/ControlPanel/Views/Banner/index.php?page=".($currentPage+1)."' class='button'>>></a></li>";
//        if($currentPage>$totalPages){
//            echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/ControlPanel/Views/Banner/index.php?page=".$currentPage."' class='button'>>></a></li>";
//
//        }
        ?>
    </ul>
</div>



<?php
$content = ob_get_contents();
ob_end_clean();
$layout=file_get_contents('http://localhost/GreenValley/ControlPanel/Views/ThemeA/layout.php');
$output=str_replace('##CONTENT##',$content,$layout);
echo $output;
?>
