<?php


namespace Joya\Registration;
use \PDO;
use Joya\Utility\Message;
use Joya\Utility\Debug;
use Joya\Utility\AppConfig;

class Registration
{
    private $dbh = null;
    function __construct(){
        //connecting to database
//connecting to database
        try{
            $this->dbh = new PDO("mysql:host=".AppConfig::HOST."; dbname=".AppConfig::DB,AppConfig::USER,AppConfig::PASSWORD);

        }
        catch(\PDOException $exception){
            echo "Sorry unwanted issue is found. Try again later.";
        }
    }
    public function index(){

        //$dbh = new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");
        $query= "SELECT `id`, `name`, `email`, `phone` FROM registration  ORDER BY `created_at` DESC";
        return $this->dbh->query($query);
    }

    public function store($data)
    {
      // $hashed_password=password_hash($data['password'],PASSWORD_BCRYPT);

        //prepare the query
        $query = "INSERT INTO `registration` (`name`, `email`, `password`, `phone`, `created_at`, `modified_at`)
        VALUES ('$data[name]','$data[email]',' $data[password]','$data[phone]', '$data[created_at]', '$data[modified_at]')";
//echo $query;

        $result = $this->dbh->query($query);
        $message = new Message();
        if ($result) {

            $message->set("Data is inserted sucessfully.");
            header("location:http://localhost/GreenValley/ControlPanel/Views/Registration/index.php");
        } else {
            $message->set("Data is not inserted.");
        }
    }

    public function show($id = null){
        $message = new Message();
        if(is_null($id)|| !is_numeric($id)){
            $message->set("Information are not available.");
            header("location:index.php");
        }


        //prepare query
        $query= "SELECT * FROM registration WHERE id =".$_GET['id'];
        $result = $this->dbh->query($query);
        foreach($result as $row){
            $registration = $row;
        }

        if(is_null($registration)){
            $message->set("Information are not available.");
            header("location:index.php");
        }
        else{
            return $registration;
        }
    }

    public function destroy($id = null){
        $message = new Message();
        if(is_null($id)|| !is_numeric($id)){
            $message->set("Information are not available.");
            header("location:index.php");
        }

        //prepare query
        $query= "DELETE FROM registration WHERE id =".$_GET['id'];

        $result = $this->dbh->query($query);

        if($result){
            $message->set("Data is deleted sucessfully.");
            header("location:index.php");
        }
        else{
            $message->set("Data is not deleted.");
        }

    }
    public function update($data){
        $current=date('Y-m-d h:i:s',time());
        //prepare query
        $query= "UPDATE `registration` SET 
		`name`='$data[name]',
		`email`= '$data[email]',
		`password`='$data[password]',
		`phone`='$data[phone]',
		`modified_at`='$current'
		
		WHERE  `registration`.`id`=$data[id];";
        $result = $this->dbh->query($query);
        $message = new Message();
        if($result){
            $message->set("Data is updated sucessfully.");
            header("location:index.php");
        }
        else{
            $message->set("Data is not Updated.");
        }
    }

    public function signup($data)
    {

        //prepare the query
        $query = "INSERT INTO `newsletter` (`email`,`created_at`, `modified_at`)
        VALUES ('$data[email]', '$data[created_at]', '$data[modified_at]')";

        $result = $this->dbh->query($query);
        if ($result) {

            header("location:http://localhost/GreenValley/Front/Views/thank.php");
        }
    }


    public function unsubscribe($data)
    {
        $current = date('Y-m-d h:i:s', time());
        //prepare query
        $query = "UPDATE `newsletter` SET 
		`is_subscribe`= 0,
		`modified_at`='$current'
		WHERE  `newsletter`.`id`=$data[id];";
        $result = $this->dbh->query($query);
        if ($result) {
            header("location:http://localhost/GreenValley/Front/Views/index.php");
        }

    }
}


