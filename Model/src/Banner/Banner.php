<?php


namespace Joya\Banner;
use \PDO;
use Joya\Utility\Message;
use Joya\Utility\Debug;
use Joya\Utility\AppConfig;

class Banner
{
	private $dbh = null;
	
	function __construct(){
		//connecting to database
        try{
            $this->dbh = new PDO("mysql:host=".AppConfig::HOST."; dbname=".AppConfig::DB,AppConfig::USER,AppConfig::PASSWORD);
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(\PDOException $exception){
            echo "Sorry unwanted issue is found. Try again later.";
        }
	}
	public function index(){
		
		//prepare the query
		$query= "SELECT `id`, `title`, `picture`, `link`, `is_active` FROM banner WHERE  `is_delete`=0 ORDER BY `created_at` DESC";
		$stmt=$this->dbh->prepare($query);
		//$stmt->bindParam();
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
		//return $this->dbh->query($query);
	}
	
	public function store($data){
		
		//prepare the query

		$query= "INSERT INTO `banner` (`title`, `picture`, `link`, `is_active`, `created_at`, `modified_at`)
		VALUES (:title,:picture,:link,:is_active, :created_at, :modified_at)";
        try{
            $stmt=$this->dbh->prepare($query);
            $stmt->bindParam('title',$data['title']);
            $stmt->bindParam('picture',$_FILES['picture']['name']);
            $stmt->bindParam('link',$data['link']);
            $stmt->bindParam('is_active',$data['is_active']);
            $stmt->bindParam('created_at',$data['created_at']);
            $stmt->bindParam('modified_at',$data['modified_at']);
            $stmt->execute();

            $message = new Message();
            if($stmt){

                $message->set("Banner is inserted sucessfully.");
                header("location:index.php");
            }
            else{
                $message->set("Banner is not inserted.");
            }
        }catch (PDOException $e){
            echo "An exception has occured. Please try again later or contact with admin.";
        }



	}
	
	public function show($id = null){
		$message = new Message();
		if(is_null($id)|| !is_numeric($id)){
			$message->set("Information are not available.");
            header("location:http://localhost/GreenValley/ControlPanel/Views/home.php");
		}
		
		
		//prepare query
		$query= "SELECT * FROM banner WHERE id =?";
        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute([$id]);
        return $stmt->fetch(\PDO::FETCH_ASSOC);

		if(is_null($stmt)){
			$message->set("Information are not available.");
			header("location:http://localhost/GreenValley/ControlPanel/Views/home.php");
		}
		else{
			return $stmt;
		}
	}


    public function destroy($id = null){
        $message = new Message();
        if(is_null($id)|| !is_numeric($id)){
            $message->set("Information are not available.");
            header("location:trash.php");
        }

        //prepare query
        $query= "DELETE FROM banner WHERE id = ?";
        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute([$id]);
        if($stmt){
            $message->set("Banner is deleted permanently.");
            header("location:trash.php");
        }
        else{
            $message->set("Banner is not deleted.");
        }
    }
	
	public function softDestroy($id = null){
		$message = new Message();
		if(is_null($id)|| !is_numeric($id)){
			$message->set("Information are not available.");
			header("location:index.php");
		}
		
        $query= "UPDATE `banner` SET `is_delete` = 1 
		WHERE  `banner`.`id`=".$_GET['id'];
        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute();
		//$result = $this->dbh->query($query);

		if($stmt){
				$message->set("Banner is deleted sucessfully.");
				header("location:index.php");
		}
		else{
				$message->set("Banner is not deleted.");
		}
	}


    public function restore($id = null){
        $message = new Message();
        if(is_null($id)|| !is_numeric($id)){
            $message->set("Information are not available.");
            header("location:index.php");
        }

        $query= "UPDATE `banner` SET `is_delete` = 0
		WHERE  `banner`.`id`=".$_GET['id'];
        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute();
        if($stmt){
            $message->set("Banner is restored sucessfully.");
            header("location:index.php");
        }
        else{
            $message->set("Banner is not restored.");
        }
    }


	
	public function update($data){
        $current=date('Y-m-d h:i:s',time());
		//prepare query
		$query= "UPDATE `banner` SET 
				`title`='$data[title]',
				`link`='$data[link]',
				`is_active`='$data[is_active]',
				`picture` = '$data[picture]',
				`modified_at` = '$current'
				WHERE  `banner`.`id`= $data[id];";
        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute();

		$message = new Message();
		if($stmt){
			$message->set("Banner is updated sucessfully."); 
			header("location:index.php");
		}
		else{
			$message->set("Banner is not Updated.");
		}
	}
	

	public function bannerActive(){
		//prepare the query
		$query= "SELECT `id`, `title`, `picture`, `link` FROM `banner` WHERE  `is_active`=1 ORDER BY `created_at` DESC;";
        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }

	public function activate(){
        //prepare query
        $query= "UPDATE `banner` SET `is_active` = 1 
		WHERE  `banner`.`id`=".$_GET['id'];

        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute();
        $message = new Message();

        if($stmt){
            $message->set("Banner is Activate.");
            header("location:index.php");
        }
        else {
            $message->set("Banner is not Activate.");
        }
    }

    public function deactivate(){
        //prepare query
        $query= "UPDATE `banner` SET `is_active` = 0 
		WHERE  `banner`.`id`=".$_GET['id'];
        $stmt=$this->dbh->prepare($query);
        //$stmt->bindParam();
        $stmt->execute();
        $message = new Message();

        if($stmt){
            $message->set("Banner is Deactivate.");
            header("location:index.php");
        }
        else{
            $message->set("Banner is not Deactivate.");
        }
    }



    public function isDelete(){
        //prepare the query
        $query= "SELECT `id`, `title`, `picture`, `link` FROM `banner` WHERE  `is_delete`=1 ORDER BY `created_at` DESC;";
//        $stmt=$this->dbh->prepare($query);
//        //$stmt->bindParam();
//        $stmt->execute();
        return $this->dbh->query($query);
    }


}