<?php


namespace Joya\Utility;


class Debug
{
    public static function d($vars){

        echo "<pre>";
        print_r($vars);
        echo "</pre>";
    }

    public static function dd($vars){
        self::d($vars);
        die();
        exit();
    }
}