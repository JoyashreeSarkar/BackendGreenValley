<?php


namespace Joya\Utility;


class AppConfig
{
    const HOST='localhost';
    const DB='greenvalley';
    const USER='root';
    const PASSWORD='';
    const BACK_ELEMENT_PATH = '/GreenValley/ControlPanel/Views/Elements/';


     public function backElementPath($fileName){
           include_once ($_SERVER['DOCUMENT_ROOT'].self::BACK_ELEMENT_PATH.$fileName);
     }



    const FRONT_ELEMENT_PATH = '/GreenValley/Front/Views/Elements/';


    public function frontElementPath($fileName){
        include_once ($_SERVER['DOCUMENT_ROOT'].self::FRONT_ELEMENT_PATH.$fileName);
    }


}