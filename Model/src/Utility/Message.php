<?php


namespace Joya\Utility;


class Message
{

    public function set($message){
        $_SESSION['message'] = $message;
        return true;
    }

    public function get(){
        if(array_key_exists('message',$_SESSION )
            && !empty($_SESSION['message'])){
            $message = $_SESSION['message'];
            $_SESSION['message'] = "";
            return $message;
        }else{
            return "";
        }
    }

    public function has(){
        if(array_key_exists('message',$_SESSION )
            && !empty($_SESSION['message'])){
            return true;
        }else{
            return false;
        }
    }
    public function __toString()
    {
        return $this->get();
    }
}