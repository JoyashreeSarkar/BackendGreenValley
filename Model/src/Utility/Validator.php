<?php


namespace Joya\Utility;

class Validator
{

    public static function validate($var, $rule=null){

        return $var;
    }
    public static function validateEmpty($var){
        if(empty($var)){
            return true;
        }else{
            return false;
        }

    }
    public static function validateAlpha($var){
        //$pattern = "/^[a-zA-Z]+$/";
        $pattern = "/[[:alpha:]]/";
        return preg_match($pattern,$var);

    }

}