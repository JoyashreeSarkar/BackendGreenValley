<?php
/**
 * Created by PhpStorm.
 * User: Joya
 * Date: 9/13/2018
 * Time: 10:37 PM
 */

namespace Joya\Contact;
use \PDO;
use Joya\Utility\Message;
use Joya\Utility\Debug;
use Joya\Utility\AppConfig;


class Contact
{
    private $dbh = null;

    public function __construct(){
        //connecting to database
        try{
            $this->dbh = new PDO("mysql:host=".AppConfig::HOST."; dbname=".AppConfig::DB,AppConfig::USER,AppConfig::PASSWORD);

        }
        catch(\PDOException $exception){
            echo "Sorry unwanted issue is found. Try again later.";
        }
    }
    public function all_message(){

        //$dbh = new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");
        $query= "SELECT  `id`,`name`, `email`, `message`,`is_seen` FROM users ORDER BY `created_at` DESC";
        return $this->dbh->query($query);
    }

    public function store($data)
    {
        // $hashed_password=password_hash($data['password'],PASSWORD_BCRYPT);

        //prepare the query
        $query = "INSERT INTO `users` (`name`, `email`, `message`, `created_at`, `modified_at`)
        VALUES ('$data[name]','$data[email]',' $data[message]', '$data[created_at]', '$data[modified_at]')";
//echo $query;

        $result = $this->dbh->query($query);
        $message = new Message();
        if ($result) {

            //$message->set("Data is inserted sucessfully.");
            header("location:http://localhost/GreenValley/Front/Views/Contact/contact.php");
        } else {
            $message->set("Data is not inserted.");
        }
    }
    public function read()
    {
        //prepare query
        $query = "UPDATE `users` SET  `is_seen`= 1
		WHERE  `users`.`id`=".$_GET['id'];
        $result = $this->dbh->query($query);
        if ($result) {
            header("location:http://localhost/GreenValley/ControlPanel/Views/Contact/unread.php");
        }

    }
    public function unread()
    {
        //prepare query
        $query = "UPDATE `users` SET  `is_seen`= 0
		WHERE  `users`.`id`=".$_GET['id'];
        $result = $this->dbh->query($query);
        if ($result) {
            header("location:http://localhost/GreenValley/ControlPanel/Views/Contact/read.php");
        }

    }

}