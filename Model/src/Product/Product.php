<?php


namespace Joya\Product;

use Joya\Utility\Debug;
use \PDO;
use Joya\Utility\Message;
use Joya\Utility\AppConfig;
class Product
{
    private $dbh = null;
   public function __construct(){
        //connecting to database
//connecting to database
       try{
           $this->dbh = new PDO("mysql:host=".AppConfig::HOST."; dbname=".AppConfig::DB,AppConfig::USER,AppConfig::PASSWORD);

       }
       catch(\PDOException $exception){
           echo "Sorry unwanted issue is found. Try again later.";
       }
    }
   public function index(){

        //$dbh = new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");
        $query= "SELECT `id`, `product_name`, `product_code`, `picture`, `categories`, `quantity`, `price` FROM products ORDER BY `created_at` DESC";
        return $this->dbh->query($query);
    }
    public function all($options=[]){
        $products=[];
        $query=null;
        $query.= "SELECT * FROM products WHERE";// ORDER BY `created_at` DESC ";
        $query.=$this->buildClause($options);
      //  Debug::dd($options);
       //echo $query;
       // $query.=$this->buildOrder($orderOptions);
       // $query.=$this->setLimit($limitOptions);
        $rs = $this->dbh->query($query);
        if ($rs){//use try catch instead
            foreach ($rs as $product){
                $products[]=$product;
            }
        }

        return $products;
    }

//    public function search($options=[]){
//
//       //$searchTitle=$options['searchTitle'];
//        $query= "SELECT * FROM `products` WHERE ".$this->buildClause($options);
//        return $this->dbh->query($query);
//    }
    private function buildClause($options){
       $clause=" 1";
//       if(array_key_exists('searchTitle', $options)){
//           //$clause.= " AND `product_name` LIKE '%".$options['searchTitle']."%'";
//           //$clause.= " OR `product_code` LIKE '%".$options['searchTitle']."%'";
//           //$clause.= " OR `categories` LIKE '%".$options['searchTitle']."%'";
//           //$clause.= " OR `description` LIKE '%".$options['searchTitle']."%'";
//       }


//sort proccessing
        if(array_key_exists('sortBy', $options)&& $options['sortBy']=='Alphabetically: A-Z'){
            $clause.= " ORDER BY `product_name` ASC";
        }
        elseif(array_key_exists('sortBy', $options)&& $options['sortBy']=='Alphabetically: Z-A'){
            $clause.= " ORDER BY `product_name` DESC";
        }
        elseif(array_key_exists('sortBy', $options)&& $options['sortBy']=='Price: Low to High'){
            $clause.= " ORDER BY `price` ASC";
        }
        elseif(array_key_exists('sortBy', $options)&& $options['sortBy']=='Price: High to Low'){
            $clause.= " ORDER BY `price` DESC";
        }
        elseif(array_key_exists('sortBy', $options)&& $options['sortBy']=='Date: Old to New'){
            $clause.= " ORDER BY `created_at` ASC";
        }
        elseif(array_key_exists('sortBy', $options)&& $options['sortBy']=='Date: New to Old'){
            $clause.= " ORDER BY `created_at` DESC";
        }


//

        return $clause;
    }


    public function store($data)
    {

        //prepare the query
        $query = "INSERT INTO `products` (`product_name`, `product_code`, `picture`, `categories`,`quantity`, `price`, `discount`,`description`, `created_at`, `modified_at`)
        VALUES ('$data[product_name]','$data[product_code]','$data[picture]','$data[categories]', '$data[quantity]','$data[price]','$data[discount]','$data[description]','$data[created_at]', '$data[modified_at]')";

        $result = $this->dbh->query($query);
        $message = new Message();
        if ($result) {

            $message->set("Product is inserted sucessfully.");
            header("location:index.php");
        } else {
            $message->set("Product is not inserted.");
        }
    }

    public function show($id = null){
        $message = new Message();
        if(is_null($id)|| !is_numeric($id)){
            $message->set("Information are not available.");
            header("location:index.php");
        }

        //prepare query
        $query= "SELECT * FROM products WHERE id =".$_GET['id'];
        $result = $this->dbh->query($query);
        foreach($result as $row){
            $product = $row;
        }

        if(is_null($product)){
            $message->set("Information are not available.");
            header("location:index.php");
        }
        else{
            return $product;
        }
    }

    public function destroy($id = null){
        $message = new Message();
        if(is_null($id)|| !is_numeric($id)){
            $message->set("Information are not available.");
            header("location:index.php");
        }

        //prepare query
        $query= "DELETE FROM products WHERE id =".$_GET['id'];

        $result = $this->dbh->query($query);

        if($result){
            $message->set("Product is deleted sucessfully.");
            header("location:index.php");
        }
        else{
            $message->set("Product is not deleted.");
        }

    }
    public function update($data){
        $current=date('Y-m-d h:i:s',time());
        //prepare query
        $query= "UPDATE `products` SET
		`product_name`='$data[product_name]',
		`product_code`= '$data[product_code]',
		`picture`='$data[picture]',
		`categories`='$data[categories]',
		`quantity`='$data[quantity]',
		`price`='$data[price]',
		`discount`='$data[discount]',
		`description`='$data[description]',
		`modified_at`='$current'

		WHERE  `products`.`id`=$data[id];";
        $result = $this->dbh->query($query);
        $message = new Message();
        if($result){
            $message->set("Product is updated sucessfully.");
            header("location:index.php");
        }
        else{
            $message->set("Product is not Updated.");
        }
    }
    public function downloadExcel(){

    }

}