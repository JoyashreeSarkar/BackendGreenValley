-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.3-MariaDB-log - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for greenvalley
CREATE DATABASE IF NOT EXISTS `greenvalley` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `greenvalley`;

-- Dumping structure for table greenvalley.banner
CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `link` varchar(128) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT 0,
  `max_display` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.banner: ~11 rows (approximately)
/*!40000 ALTER TABLE `banner` DISABLE KEYS */;
INSERT INTO `banner` (`id`, `title`, `picture`, `link`, `is_active`, `is_delete`, `max_display`, `created_at`, `modified_at`) VALUES
	(3, 'title_3', 'b2.jpg', 'link_3', 1, 0, 4, '2018-07-31 23:48:14', '2018-08-10 07:07:14'),
	(52, 'dfrg', 'b4.jpg', 'gfglink', 1, 0, NULL, '2018-08-07 11:13:26', '2018-08-10 07:04:35'),
	(53, 'fgcfgh', 'b3.png', 'ghvh', 1, 0, NULL, '2018-08-10 07:05:39', '2018-08-10 07:06:33'),
	(54, 'vegetables', 'vegetables1.jpg', 'ghjhgu', 1, 0, NULL, '2018-08-10 07:08:14', '2018-08-11 04:21:42'),
	(55, 'mangoes', 'mangoes.jpg', 'gvhvh', 0, 0, NULL, '2018-08-10 07:09:04', '2018-08-10 07:09:04'),
	(59, 'Papaya', 'papaya.jpg', 'hyftghfx', 0, 0, NULL, '2018-08-11 03:54:35', '2018-08-11 04:06:59'),
	(60, 'banner', 'pears.jpg', 'frgdryr', 0, 0, NULL, '2018-08-11 03:25:29', '2018-08-11 03:25:29'),
	(62, 'fruitsm', 'guavas.jpg', 'gvhjnguj', 0, 0, NULL, '2018-08-16 03:59:45', '2018-08-16 03:59:45'),
	(63, 'Blueberries', 'blueberries.jpg', 'ghnjgvjgvjmgh', 0, 0, NULL, '2018-08-18 03:31:52', '2018-08-18 03:32:34'),
	(65, 'slider', 'berries.jpg', 'fgjngkm', 0, 0, NULL, '2018-08-23 06:45:09', '2018-08-23 06:45:09'),
	(66, 'Passion', 'passion.jpg', 'ghjgvj', 0, 1, NULL, '2018-08-28 07:05:30', '2018-08-28 07:05:30');
/*!40000 ALTER TABLE `banner` ENABLE KEYS */;

-- Dumping structure for table greenvalley.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `link` varchar(128) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.categories: ~10 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `title`, `picture`, `link`, `created_at`, `modified_at`) VALUES
	(1, 'Cancer', 'cancer.jpg', 'cancer_products.php', '2018-09-03 17:25:54', '2018-09-03 17:25:56'),
	(2, 'Heart Attack', 'heartattack.jpg', 'heartattack_products.php', '2018-09-03 17:34:44', '2018-09-03 17:34:45'),
	(3, 'Diabetes', 'diabetes.jpg', 'diabetes_products.php', '2018-09-03 17:44:49', '2018-09-03 17:44:51'),
	(4, 'Pregnant Lady', 'pregnancy.jpg', 'pregnantlady_products.php', '2018-09-03 17:45:07', '2018-09-03 17:45:04'),
	(5, 'Low Blood Pressure', 'lowbp.jpg', 'lowbp_products.php', '2018-09-03 17:59:19', '2018-09-03 17:59:20'),
	(6, 'High Blood Pressure', 'highbp.jpg', 'highbp_products.php', '2018-09-03 18:00:05', '2018-09-03 18:00:06'),
	(7, 'Brain Anserum\r\n', 'brainanserum.jpg', 'brainanserum_products.php', '2018-09-03 20:06:26', '2018-09-03 20:07:46'),
	(8, 'Hepaticies C', 'hepatitisc.jpg', 'hepaticiesc_products.php', '2018-09-03 20:08:52', '2018-09-03 20:08:53'),
	(9, 'Lung Cancer', 'lungcancer.jpg', 'lungcancer_products.php', '2018-09-03 20:11:50', '2018-09-03 20:11:52'),
	(10, 'View All', 'viewall.png', 'all_products.php', '2018-09-03 20:14:13', '2018-09-03 20:14:15');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table greenvalley.contacts
CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.contacts: ~0 rows (approximately)
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;

-- Dumping structure for table greenvalley.map_categories_products
CREATE TABLE IF NOT EXISTS `map_categories_products` (
  `categories_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  KEY `FK__categories` (`categories_id`),
  KEY `FK__products` (`products_id`),
  CONSTRAINT `FK__categories` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `FK__products` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.map_categories_products: ~0 rows (approximately)
/*!40000 ALTER TABLE `map_categories_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `map_categories_products` ENABLE KEYS */;

-- Dumping structure for table greenvalley.newsletter
CREATE TABLE IF NOT EXISTS `newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `is_subscribe` tinytext NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `modified_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.newsletter: ~0 rows (approximately)
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
INSERT INTO `newsletter` (`id`, `email`, `is_subscribe`, `created_at`, `modified_at`) VALUES
	(1, 'joya@yahoo.com', '1', '2018-09-03 05:15:00', '2018-09-03 05:15:00'),
	(2, 'abc@abc.com', '1', '2018-09-05 05:12:12', '2018-09-05 05:12:12');
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;

-- Dumping structure for table greenvalley.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) DEFAULT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `categories` varchar(50) DEFAULT NULL,
  `quantity` int(20) DEFAULT NULL,
  `price` int(20) DEFAULT NULL,
  `discount` int(20) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `modified_at` datetime DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.products: ~8 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `product_name`, `product_code`, `picture`, `categories`, `quantity`, `price`, `discount`, `description`, `created_at`, `modified_at`) VALUES
	(1, 'Mango', 'p1', 'mangoes.jpg', 'Pregnent Lady', 2, 150, 0, 'ghnfghf', '2018-08-11 21:44:49', '2018-08-11 21:44:54'),
	(3, 'banana', 'p3', 'banana.png', 'Cancer', 5, 60, 0, 'jghjhgjkuj', '2018-08-12 07:31:58', '2018-08-12 07:04:17'),
	(4, 'blueberries', 'p4', 'blueberries.jpg', 'Heart Attack', 7, 150, 0, 'cgbdfhfgth', '2018-08-12 07:39:44', '2018-08-12 07:39:44'),
	(5, 'Apple', 'p5', 'apples.jpg', 'Pregnant Lady', 6, 160, 0, ' The health benefits of apples include prevention of heart, stomach, and liver disorders, gallstones, constipation, anemia, and diabetes. They also lower your risk of suffering from rheumatism, eye disorders, a variety of cancers, gout, and Alzheimer’s and Parkinson’s diseases.\r\n                    Apples help in reducing weakness, providing relief from dysentery, and promoting digestion. Finally, they also aid in dental and skin care.', '2018-08-12 07:48:57', '2018-08-12 07:48:57'),
	(6, 'Grapes', 'p6', 'grapes.jpg', 'Hepaticies C', 9, 180, 0, 'gh jnvbhnc fvb', '2018-08-12 08:13:06', '2018-08-12 08:13:06'),
	(7, 'Avocado', 'p7', 'avocado.jpg', 'Cancer', 9, 160, 0, 'hjkhjkhbk,hbjkl;l', '2018-08-12 06:27:26', '2018-08-12 06:27:26'),
	(8, 'Guavas', 'p56', 'guavas.jpg', 'Diabetes', 6, 100, 0, 'xdfnjmgyhhg', '2018-09-02 05:08:56', '2018-09-02 05:08:56'),
	(9, 'Avocado', 'p57', 'avocado.jpg', 'High Blood Pressure', 9, 160, 0, 'dsgdfhftgvujgy', '2018-09-05 01:28:50', '2018-09-05 01:28:52'),
	(10, 'Papaya', 'p58', 'papaya.jpg', 'Heart Attack', 10, 90, 0, 'Protects against Heart Disease', '2018-09-06 03:40:24', '2018-09-06 03:40:24');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table greenvalley.registration
CREATE TABLE IF NOT EXISTS `registration` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `modified_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.registration: ~12 rows (approximately)
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
INSERT INTO `registration` (`id`, `name`, `email`, `password`, `phone`, `created_at`, `modified_at`) VALUES
	(1, 'Joya', 'js@gmail.com', 'joya123', '01867104159', '2018-07-31 12:16:50', '2018-08-10 07:13:03'),
	(15, 'Utpal', 'utpal@yahoo.com', 'utpal', '01814130201', '2018-08-08 05:04:01', '2018-08-08 05:04:01'),
	(16, 'Irin', 'irin@dfkgh.fgb', 'irin', '01756118999', '2018-08-08 05:58:37', '2018-08-08 05:58:37'),
	(18, 'Rashna', 'rashna@hgfgh.com', '098hijh', '0167879879', '2018-08-08 06:01:36', '2018-08-16 03:57:23'),
	(22, 'Owakil', 'dgd@hfgvh.ghg', 'vghvghv', '0178654634', '2018-08-16 03:58:39', '2018-08-16 03:58:39'),
	(23, 'Joyashree', 'j@gmail.com', 'joya', '01818743565', '2018-09-04 05:26:16', '2018-09-04 05:26:16'),
	(24, 'Soma', 'sm@gmail.com', 'soma', '078654634543', '2018-09-04 05:29:10', '2018-09-04 05:29:10'),
	(25, 'Chandrika', 'sinha@gmail.com', ' ', '986745387', '2018-09-04 06:07:29', '2018-09-04 06:07:29'),
	(26, 'Joyashree Sarkar', 'jfhg@gmai.com', ' $2y$10$15BYvvwGV1fJrFD2JkrAiOc0Z8WmCcd1W9mvBk71tGYuzPVQR3L7.', '01818743565', '2018-09-05 05:21:52', '2018-09-05 05:21:52'),
	(27, 'Joyashree Sarkar', 'jfhg@gmai.com', ' $2y$10$15BYvvwGV1fJrFD2JkrAiOc0Z8WmCcd1W9mvBk71tGYuzPVQR3L7.', '01818743565', '2018-09-05 05:21:52', '2018-09-05 05:21:52'),
	(28, 'dfgdfg', 'abcd@djfh.com', '$2y$10$y/EqzCy8gJIsmWFuY/u5D..LhtioJNF1/6TcExdZEfexUdiHxrTLO', '01814130201', '2018-09-05 05:26:13', '2018-09-05 05:26:13'),
	(29, 'masum', 'm@b.c', ' $2y$10$29Xd/kuNdUdDb7uCNXxCs.FVlmIJFLTDau6dnE1v9AFjqm113H1ly', '01814130201', '2018-09-05 05:48:16', '2018-09-05 05:48:16'),
	(30, 'Maliha', 'maliha@gmail.com', ' $2y$10$Q5DDZzmQ3F47cSCzi3dCauSdBgVImwxgVDSwPtDBUMFRjEL26itGa', '01687865796', '2018-09-06 08:34:24', '2018-09-06 08:34:24');
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;

-- Dumping structure for table greenvalley.searchcontent
CREATE TABLE IF NOT EXISTS `searchcontent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `module` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `content` (`content`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table greenvalley.searchcontent: ~0 rows (approximately)
/*!40000 ALTER TABLE `searchcontent` DISABLE KEYS */;
/*!40000 ALTER TABLE `searchcontent` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
