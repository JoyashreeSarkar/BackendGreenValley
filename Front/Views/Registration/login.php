<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . "/GreenValley/Model/vendor/Autoload.php";
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();

?>

<!doctype html>
<html lang="en">
<?php echo $appConfig->frontElementPath("head.php"); ?>
<body>

<!--Pre Loader-->
<div class="pre-loader"><div class="loader"></div></div>



<?php echo $appConfig->frontElementPath("header.php"); ?>
</hr>


<div class="container">
    <div class="col-sm-4 col-md-4">
	<h2 style="color:#1b6d85">CUSTOMER LOGIN</h2>

	</div>
<div class="col-sm-8 col-md-8">
<section class="register">
<form method="post" action="account.php">
<div class="reg_section">

	<input type="email" name="email" value="" placeholder="E-mail Address containing '@' and '.'"></br>
	<input type="password" name="password" value="" placeholder="password">
</div>
	<p class="submit"><input type="submit" value="SIGN IN" name="button">New Customer?<a href="register.php">Sign up</a></p>

</form>
</section>
</div>
</div>




<?= $appConfig->frontElementPath('footer.php'); ?>



<!--This is JQUARY -->
<script src="js/jquery-3.2.1.min.js"></script>

<!--This is Bootstrap-4 JS-->
<script src="js/bootstrap.min.js"></script>
<script>
			function f1()
			{	
				document.getElementById('pop').style.display="block";
			}
			function f2()
			{	
				document.getElementById('pop').style.display="none";

			}

		</script>




<!--Page Loader-->
<script>
    setTimeout(function () {
        $('.pre-loader').fadeToggle();
    }, 1500);
</script>
</body>
</html>