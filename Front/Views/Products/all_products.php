<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . "/GreenValley/Model/vendor/Autoload.php";

use Joya\Utility\AppConfig;
use Joya\Utility\Debug;
use Joya\Product\Product;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
$objProduct=new Product();
//$options= [];
if(strtolower($_SERVER['REQUEST_METHOD'])=='post'){
    $products=$objProduct->all($_POST); //taking direct user input is dangerous
    //$searchTitle=$_POST['searchTitle'];
}
else{
    $products=$objProduct->all();
    // $searchTitle='';
}

if (array_key_exists('page', $_GET)) {
    $currentPage = $_GET['page'];
} else {
    $currentPage = 1;
}
$adapter = new ArrayAdapter($products);
//var_dump($adapter);
//die();
$pagerfanta = new Pagerfanta($adapter);
$pagerfanta->setMaxPerPage(3); // 10 by default
$maxPerPage = $pagerfanta->getMaxPerPage();

$pagerfanta->setCurrentPage($currentPage); // 1 by default
$currentPage = $pagerfanta->getCurrentPage();

$nbResults = $pagerfanta->getNbResults();//total records in database
$currentPageResults = $pagerfanta->getCurrentPageResults();//result based on current page number
$appConfig = new AppConfig();



?>
<!doctype html>
<html lang="en">

<?php echo $appConfig->frontElementPath("head.php"); ?>

<body>


<?php echo $appConfig->frontElementPath("header.php"); ?>
<!--shop start-->
<section>
    <div class="container">
        <h2 class="text-center shop-heading">VIEW ALL FRUITS</h2>
        <div class="col-lg-12">
        <?php echo $appConfig->frontElementPath("product_sidebar.php"); ?>

        <div class="col-lg-9">
            <form method="post" action="http://localhost/GreenValley/Front/Views/Products/all_products.php">
                <?php echo $appConfig->frontElementPath("sort_by.php"); ?>



<!--                <table>-->
<!--                    <tr>-->
<!--                        <td>-->
<!--                            <input type="text" placeholder="Search...." name="searchTitle" value="--><?php //echo $searchTitle ?><!--">-->
<!--                        </td>-->
<!--                        <td>-->
<!---->
<!--                        </td>-->
<!--                        <td>-->
<!--                            <button type="submit">Search</button>-->
<!--                        </td>-->
<!--                    </tr>-->
<!--                </table>-->
            </form>

            <?php
            if(count($products)>0):
        foreach ($currentPageResults as $product):
            ?>
            <div class="row_image">

                <div class="hover04 column">

                    <div class="col-md-4">
                        <a href="http://localhost/GreenValley/Front/Views/Products/product_details.php?id=<?= $product['id'] ?>">
                            <figure>
                        <img src="http://localhost/GreenValley/products/<?= $product['picture'] ?>" height="300"
                             width="100%" alt="product_pic">
                            </figure>
                         </a>

                            <?= $product['product_name'] ?>
                            <br>
                            price:<?= $product['price'] ?>taka per kg/dozzon<br>
                            <a href="http://localhost/GreenValley/Front/Views/Cart/addtocart.php" class="btn blue">QUICK SHOP</a><br>
                        </div>


                </div>
            </div>

        <?php
        endforeach;
        else:
        ?>
            <div class="alert-warning">No product is found</div>
            <?php
            endif;
            ?>
        </div>
        </div>
        <div class="pagination">
            <ul class="pagination">
                <?php
                $pageNumber=1;
                if($currentPage > 0) {
                    echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/Front/Views/Products/all_products.php?page=" . ($currentPage - 1) . "' class='button'><<</a></li>";
                }else {
                    echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/Front/Views/Products/all_products.php?page=" . $currentPage . "' class='button'><<</a></li>";
                }
                $totalPages = ceil($nbResults / $maxPerPage);
                if ($pagerfanta->haveToPaginate()) {
                    for ($pageNumber; $pageNumber <= $totalPages; $pageNumber++) {
                        echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/Front/Views/Products/all_products.php?page=" . $pageNumber . "'>" . $pageNumber . "</a></li>";
                    }
                } else {
                    echo "<li>No pagination required</li>";
                }
                echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/Front/Views/Products/all_products.php?page=".($currentPage+1)."' class='button'>>></a></li>";
                if($currentPage>$totalPages){
                    echo "<li style='font-size: x-large; padding: 5px;'><a href='http://localhost/GreenValley/Front/Views/Products/all_products.php?page=".$currentPage."' class='button'>>></a></li>";

                }
                ?>
            </ul>
        </div>
    </div>

</section>


<?= $appConfig->frontElementPath('footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>


</body>
</html>