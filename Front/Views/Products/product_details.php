<?php
include_once $_SERVER['DOCUMENT_ROOT']."/GreenValley/Model/vendor/Autoload.php";
$dbh=  new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();
use Joya\Product\Product;
$objproduct = new Product();
$query= "SELECT * FROM products WHERE `id`=".$_GET['id'];
$products = $dbh->query($query);
//var_dump($products);

//$product = $objproduct->show($_GET['id']);

?>
<!doctype html>
<html lang="en">
<?php echo  $appConfig->frontElementPath("head.php"); ?>
	<body>
    <!-- Navbar-->
    <?php echo $appConfig->frontElementPath("header.php"); ?>

<!--shop start-->
		<section>
			<div class="container">
				<div class="row">
				<h2 class="text-center shop-heading">Product Details</h2>

			<div class="col-md-6">
                <?php
                foreach($products as $product):
                ?>
                <img src="http://localhost/GreenValley/products/<?= $product['picture'] ?>" height="300" width="400" alt="pic"
                     border-radious="50">
                    <?php
                endforeach;
                ?>
            </div>
					<div class="col-md-6 product-right">
                        <div><?= $product['product_name'] ?>
                        </div>
						<div>
                            <label for="pro_price">Price</label>
                            <p class="pro-price"><?= $product['price'] ?>tk</p>
                        </div>
						<div>
							<label for="quantity">Quantity</label>
						<input id="quantity" class="quantity" min="1" size="2" name="quantity" value="1" type="number">
                        </div>
                        <div >
                            <p class="pro-details"><?= $product['description'] ?></p>
                        </div>
						<div>	
							<a href="http://localhost/GreenValley/Front/Views/Cart/cart.php" class="btn blue">ADD TO CART</a>
						</div>
					</div>
		</div>
			</div>
		</section>


    <?php echo  $appConfig->frontElementPath("footer.php"); ?>

    <?php echo  $appConfig->frontElementPath("js.php"); ?>

    </body>
</html>