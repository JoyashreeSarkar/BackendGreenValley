<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'] . "/GreenValley/Model/vendor/Autoload.php";
$dbh = new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");

use Joya\Utility\AppConfig;

$appConfig = new AppConfig();
$query = "SELECT * FROM products WHERE `categories`='High Blood Pressure' ORDER BY `created_at` DESC ";
$products = $dbh->query($query);
?>
<!doctype html>
<html lang="en">

<?php echo $appConfig->frontElementPath("head.php"); ?>

<body>


<?php echo $appConfig->frontElementPath("header.php"); ?>
<!--shop start-->
<section>
    <div class="container">
        <h2 class="text-center shop-heading">ALL HIGH BLOOD PRESSURE FRUITS</h2>
        <?php echo $appConfig->frontElementPath("product_sidebar.php"); ?>

        <div class="col-lg-9">
            <?php echo $appConfig->frontElementPath("sort_by.php"); ?>

            <?php
            foreach ($products as $product):
                ?>
                <div class="row_image">

                    <div class="hover04 column">

                        <div class="col-md-4">
                            <a href="http://localhost/GreenValley/Front/Views/Products/product_details.php?id=<?= $product['id'] ?>">
                                <figure>
                                    <img src="http://localhost/GreenValley/products/<?= $product['picture'] ?>" height="300"
                                         width="100%" alt="product_pic">
                                </figure>
                            </a>

                            <?= $product['product_name'] ?>
                            <br>
                            price:<?= $product['price'] ?>taka per kg/dozzon<br>
                            <a href="http://localhost/GreenValley/Front/Views/Cart/addtocart.php" class="btn blue">QUICK SHOP</a><br>
                        </div>


                    </div>
                </div>

            <?php
            endforeach;
            ?>
        </div>
    </div>
</section>


<?= $appConfig->frontElementPath('footer.php'); ?>

<!-- JS -->
<script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>


</body>
</html>