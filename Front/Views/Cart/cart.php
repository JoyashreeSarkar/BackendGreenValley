<?php
include_once $_SERVER['DOCUMENT_ROOT']."/GreenValley/Model/vendor/Autoload.php";
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();
$dbh = new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");
$query2 = "SELECT * FROM categories  ORDER BY  `title` ASC LIMIT 0,3";
$products2 = $dbh->query($query2);
?>
<!doctype html>
<html lang="en">
<?= $appConfig->frontElementPath('head.php'); ?>
    <body>


	    <!-- Navbar-->
        <?= $appConfig->frontElementPath('header.php'); ?>
        <h2 class="text-center shop-heading">Shopping Cart</h2><br>
        <p style="font-size:20px; text-align: center"><i>There are no items in your cart.<a href="http://localhost/GreenValley/Front/Views/Products/all_products.php">Continue Shopping → </a></i></p>


<!--        <div class="main-wrapper">-->
<!---->
<!--            <div id="vue">-->
<!--                <!--            <div class="cart"><span class="cart-size"> 0 </span><i class="fa fa-shopping-cart"></i>-->
<!--                <!--				<div class="cart-items" style="display: none;">-->
<!--                <!--				<table class="cartTable"><tbody></tbody></table>-->
<!--                <!--				<h4 class="cartSubTotal" style="display: none;"> $0.00 </h4>-->
<!--                <!--				<button class="clearCart" style="display: none;"> Clear Cart </button>-->
<!--                <!--				<button class="checkoutCart" style="display: none;"> Checkout </button>-->
<!--                <!--				<table></table>-->
<!--                <!--				</div>-->
<!--                <!--			</div>-->
<!---->
<!---->
<!--            </div>-->
<!---->
<!--        </div>-->
        <div class="container col-lg-12">

            <?php
            foreach ($products2 as $product):
                ?>
                <div class="row_image">
                    <li>
                        <a href="http://localhost/GreenValley/Front/Views/Products/<?= $product['link'] ?>">
                            <img src="http://localhost/GreenValley/images/<?= $product['picture'] ?>" height="180" width="100%">
                        </a>
                    </li>
                </div>

            <?php
            endforeach;
            ?>
        </div>



        <?= $appConfig->frontElementPath('footer.php'); ?>


    <!-- JS -->
    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/cart.js"></script>
    </body>
</html>