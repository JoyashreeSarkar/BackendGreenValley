<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/GreenValley/Model/vendor/Autoload.php";
use Joya\Utility\AppConfig;

$appConfig = new AppConfig();
?>
<!doctype html>
<html lang="en">
<?= $appConfig->frontElementPath('head.php'); ?>
    <body>
    <!-- Navbar-->
    <?= $appConfig->frontElementPath('header.php'); ?>

<!--shop start-->
<section>
    <div class="container">
        <div class="row">
        <h2 class="text-center shop-heading">Product Details</h2>

    <div class="col-md-6">
        <img src="images/fruits/apples.jpg" alt="apple" height="400px" class="pro-img">
    </div>
            <div class="col-md-6 product-right">
<div><p class="product-title">APPLE</p></div>
                <div><p class="pro-price">14.05</p></div>
                <div>
                    <label for="quantity">Qty</label>
                <input id="quantity" class="quantity" min="1" size="2" name="quantity" value="1" type="number"></div>
                <div>
                    <a href="../cart%20details.php" class="btn blue">ADD TO CART</a>
                </div>
                <div class="pro-details">
                    The health benefits of apples include prevention of heart, stomach, and liver disorders, gallstones, constipation, anemia, and diabetes. They also lower your risk of suffering from rheumatism, eye disorders, a variety of cancers, gout, and Alzheimer’s and Parkinson’s diseases.
                    Apples help in reducing weakness, providing relief from dysentery, and promoting digestion. Finally, they also aid in dental and skin care.
                </div>
            </div>
</div>
    </div>
</section>


    <?= $appConfig->frontElementPath('footer.php'); ?>


    <!-- JS -->
    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	

    </body>
</html>