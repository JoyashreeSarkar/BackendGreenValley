<!doctype html>
<html lang="en">
    <?php include ("head.php"); ?>
    <body>
    <!-- Navbar-->
    <?php include ("header.php"); ?>

<!--shop start-->
<section>
    <div class="container">
        <div class="row">
        <h2 class="text-center shop-heading">Product Details</h2>

    <div class="col-md-6">
        <img src="images/fruits/apples.jpg" alt="apple" height="400px" class="pro-img">
    </div>
            <div class="col-md-6">
<div><p class="product-title">APPLE</p></div>
                <div><p class="pro-price">14.05</p></div>
                <div>
                    <label for="quantity">Qty</label>
                <input id="quantity" class="quantity" min="1" size="2" name="quantity" value="1" type="number"></div>
                <div><button class="remove" style="background-color: white;border: white;margin-top: 4px">Remove</button></div>
                <div>
                    <a href="login.php" class="btn blue" style="padding: 5px 35px">CheckOut</a>
                </div>

            </div>
</div>
    </div>
</section>

<?php include ("footer.php"); ?>



    <!-- JS -->
    <script type="text/javascript" src="js/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	

    </body>
</html>