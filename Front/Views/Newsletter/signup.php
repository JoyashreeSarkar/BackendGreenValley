<?php
session_start();
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';

use Joya\Utility\Validator;
use Joya\Registration\Registration;

$data=[];
$data['email'] = Validator::validate($_POST['email']);
$data['created_at']=date('Y-m-d h:i:s',time());
$data['modified_at']=date('Y-m-d h:i:s',time());

$registration = new Registration();
$registration->signup($data);