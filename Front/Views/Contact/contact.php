<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();
?>
<!doctype html>
<html lang="en">
<?= $appConfig->frontElementPath('head.php'); ?>
<body>

<!--Pre Loader-->
<div class="pre-loader"><div class="loader"></div></div>




<?= $appConfig->frontElementPath('header.php'); ?>
</hr>

<div class="container contact">
    <div class="col-sm-4 col-md-4">
	<h2 class="text-center shop-heading">CONTACT</h2>
	<P>At GREEN VALLEY, we love to hear from our customers.</P>
	<P>If you would like to chat, please e-mail us at: <a href="#">joyac113227@gmail.com</a></P>

	</div>
	<div class="col-sm-8 col-md-8">
	<section class="register">
		<form action="http://localhost/GreenValley/ControlPanel/Views/Contact/store.php" method="POST">
			<div class="reg_section">
				<div><label for="name">Full Name :</label></div>
				<div><input type="text" id="name" name="name" value=""></div>
				<div><label for="email">Email :</label></div>
				<div><input type="email" id="email" name="email" value=""></div>
				<div><label for="textarea">Your Message :</label></div>
				<div><textarea name="message" id="textarea"></textarea></div>
				<p class="submit"><input type="submit" value="SEND" name="button"></p>
			</div>
		</form>
	</section>
	</div>
</div>



<?= $appConfig->frontElementPath('footer.php'); ?>




<?= $appConfig->frontElementPath('js.php'); ?>





<!--Page Loader-->
<script>
    setTimeout(function () {
        $('.pre-loader').fadeToggle();
    }, 1500);
</script>
</body>
</html>