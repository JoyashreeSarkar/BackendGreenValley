<?php
include_once $_SERVER['DOCUMENT_ROOT']."/GreenValley/Model/vendor/Autoload.php";
$dbh=  new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");
use Joya\Utility\AppConfig;
$appConfig = new AppConfig();
$query= "SELECT * FROM categories ";
$products = $dbh->query($query);
?>
<header>


    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="brand"><a href="index.php"><h1>GREEN VALLEY</h1></a></div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">  <ul class="links nav navbar-nav" >

                    <li class="menu">
                        <a>CATEGORIES<img src="http://localhost/GreenValley/images/arrow_drop.png" class="login" alt="" width="20px" height="20px"></a>

                        <ul class="dropmenu">
                            <?php
                            foreach ($products as $product):
                            ?>
                            <li><a href="http://localhost/GreenValley/Front/Views/Products/<?= $product['link'] ?>"><?= $product['title'] ?></a></li>
                            <?php
                            endforeach;
                            ?>
                        </ul>
                    </li>
                    <li><a href="http://localhost/GreenValley/Front/Views/Contact/contact.php">CONTACT</a></li>
                    <li><a href="http://localhost/GreenValley/Front/Views/about.php">ABOUT US</a></li>
                    <li><a href="http://localhost/GreenValley/Front/Views/index.php">BLOG</a></li>
                    <li><a href="http://localhost/GreenValley/Front/Views/Cart/cart.php">CART</a></li>
                    <li><a href="http://localhost/GreenValley/Front/Views/Registration/register.php"><img src="http://localhost/GreenValley/images/msn-logo.png" class="login" alt="" width="50px" height="40px"></a></li>
                    <li><a href="http://localhost/GreenValley/Front/Views/Search/search.php"><img src="http://localhost/GreenValley/images/search.png" class="search" alt="search" width="50px" height="40px"></a></li>

                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

	</header>
	
	