<?php
include_once $_SERVER['DOCUMENT_ROOT'].'/GreenValley/Model/vendor/Autoload.php';
$dbh=  new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");
$query= "SELECT * FROM banner WHERE `is_active` = 1 && `is_delete` = 0 ORDER BY `created_at` DESC LIMIT 0,4";
$banners = $dbh->query($query);
$all_banners = array();

foreach($banners as $banner)
{
    $all_banners[]=$banner;
}
//echo count($all_banners);

 ?>
<div class="bt-carousel hidden-xs">
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
<!--                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>-->
<!--                <li data-target="#myCarousel" data-slide-to="1" ></li>-->
<!--                <li data-target="#myCarousel" data-slide-to="2" ></li>-->
<!--                <li data-target="#myCarousel" data-slide-to="3" ></li>-->
                <?php
                $active = 'active';
                for($i = 0;$i<count($all_banners);$i++):
                ?>
                <li data-target="#myCarousel" data-slide-to="<?=$i;?>" class="<?=$active?>"></li>
                <?php
                    $active = '';
                endfor;
                ?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">

                <?php
                $active = 'active';
                foreach($all_banners as $banner):
                ?>
                <div class="item <?=$active?>">
                    <a href="http://localhost/Greenvalley/Front/Views/Products/<?=$banner['link']?>">
                    <img src="http://localhost/GreenValley/banner/<?=$banner['picture']?>" alt="pic1" style="width:100%; " >
                    </a>
                </div>
                <?php
                  $active='';
                endforeach;
                ?>

            </div>
    </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
</div>