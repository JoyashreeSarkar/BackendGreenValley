
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="keywords" content="landing_page, coming_soon, creative, one_page, zayerTheme, bootstrap">
    <meta name="author" content="zayerTheme">

    <!-- SITE TITLE -->
    <title>Ojir's green valley</title>

    <!-- Favicons -->
    <link rel="icon" href="../images/favicon.png">
	<link rel="apple-touch-icon" href="../images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="../images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="../images/apple-touch-icon-114x114.png">
      
    <!-- CSS -->
    <link rel="stylesheet" href="http://localhost/GreenValley/Front/Assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="http://localhost/GreenValley/Front/Assets/css/style.css">


</head>