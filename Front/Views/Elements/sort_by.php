<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/GreenValley/Model/vendor/Autoload.php";
use Joya\Utility\AppConfig;
use Joya\Utility\Debug;
use Joya\Product\Product;

$appConfig = new AppConfig();

$objProduct = new Product();
//$options= [];
if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
    $products = $objProduct->all($_POST); //taking direct user input is dangerous
    $sortBy = $_POST['sortBy'];
} else {
    $products = $objProduct->all();
    $sortBy = '';
}


?>
<!--<form method="post" action="http://localhost/GreenValley/Front/Views/ThemeA/Products/all_products.php">-->
    <div style="text-align: right" id='selectedResult'> <select class="sort_div custom-select" id="categories" name="sortBy" >
            <option selected><?php echo $sortBy ?></option>
            <option value="Alphabetically: A-Z">Alphabetically: A-Z</option>
            <option value="Alphabetically: Z-A">Alphabetically: Z-A</option>
            <option value="Price: Low to High">Price: Low to High</option>
            <option value="Price: High to Low">Price: High to Low</option>
            <option value="Date: Old to New">Date: Old to New</option>
            <option value="Date: New to Old">Date: New to Old</option>
            <!--        <option name="sortBy" value="--><?php //echo $sortBy ?><!--">Best Selling</option>-->

        </select>
        <button type="submit">Sort By</button>
    </div>
<!--</form>-->

