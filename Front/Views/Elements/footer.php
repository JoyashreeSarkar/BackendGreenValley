<?php
include_once $_SERVER['DOCUMENT_ROOT'] . "/GreenValley/Model/vendor/Autoload.php";
$dbh = new PDO("mysql:host=localhost;dbname=greenvalley", "root", "");

use Joya\Utility\AppConfig;

$appConfig = new AppConfig();
$query = "SELECT * FROM categories ";
$products = $dbh->query($query);
?>
<div class="container">
    <div class="footer_wrapper row">
        <div class="row">
            <div class="col-sm-2 col-md-2 col-md-offset-1">
                <h3 style="color:#1b6d85">Links</h3>
                <div style="font-weight: bold">

                    <li><a class="ex" href="http://localhost/GreenValley/Front/Views/Products/all_products.php">SHOP</a>
                    </li>
                    <li><a class="ex" href="http://localhost/GreenValley/Front/Views/Contact/contact.php">CONTACT</a>
                    </li>
                    <li><a class="ex" href="http://localhost/GreenValley/Front/Views/about.php">ABOUT US</a></li>
                    <li><a class="ex" href="http://localhost/GreenValley/Front/Views/blog.php">BLOG</a></li>
                    <li><a class="ex" href="http://localhost/GreenValley/Front/Views/index.php">HOME</a></li>
                </div>
            </div>
            <div class="col-sm-2 col-md-2 col-md-offset-1">
            <h3 style="color:#1b6d85">Products</h3>

            <div class="col-md-6 foot-pro1"
                 style="margin-left: -70px; width: 100%; padding-left: 20px;">
                <?php
                foreach ($products as $product):
                    ?>
                    <li>
                        <a href="http://localhost/GreenValley/Front/Views/Products/<?= $product['link'] ?>"><?= $product['title'] ?></a>
                    </li>
                <?php
                endforeach;
                ?>
            </div>


        </div>
        <div class="col-sm-3 col-md-3">
            <h3 style="color:#1b6d85">Join With Us</h3>
            <div class="social-btns">
                <a class="btn facebook" href="https://www.facebook.com/Ojirs-GREEN-Valley-391602137910973/?modal=admin_todo_tour"><i class="fa fa-facebook"></i></a>
                <a class="btn twitter" href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
                <a class="btn google" href="https://google.com/"><i class="fa fa-google"></i></a>
                <a class="btn dribbble" href="https://www.instagram.com/"><i class="fa fa-instagram"></i></a>
            </div>

        </div>

        <div class="col-sm-3 col-md-3">
            <h3 style="color:#1b6d85">News & Updates</h3>
            <form method="post" action="http://localhost/GreenValley/Front/Views/Newsletter/signup.php" role="form">

                <div class="form-group row">
                    <div class="col-sm-10 offset-1">
                        Sign up to get the latest on sales, new releases and more...
                        <input type="email" class="form-control" id="email" name="email"
                               placeholder="Enter your Email....">
                        <button type="submit" class="btn btn-primary">SIGN UP</button>
                        <br>
                        <!--                        <a href="http://localhost/GreenValley/Front/Views/Newsletter/unsubscribe.php" style="background: gray; margin-left: 10px">Unsubscribe</a>-->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="footer_wrapper" style="background-color:  #f2f3f4  ">
    <div class="">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="table">
                    <div class="table_cell">
                        <p>Copyright © 2018 Green Valley
                            <br> All Rights Reserved. Designed by
                            <a href="http://localhost/GreenValley/Front/Views/about.php" target="_blank">OJIR's</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
   